<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Expense;
use App\User;

class ExpenseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        // Get all of the recurring expenses for the user
        $expenses = $user->expenses()->where('recurring', 1)
                    ->orderBy('cost', 'desc')
                    ->get();

        $totalCost = 0;

        foreach ( $expenses as $expense ) {
            $totalCost = $totalCost + floatval($expense->cost);
        }

        return view('expenses.index' , ['expenses' => $expenses, 'totalCost' => $totalCost, 'leftOver' => '500']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('expenses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get the logged in user
        $user = Auth::user();

        // Times the amount by 100 to store in the database
        $floated =  floatval($request["amount"]);

        // Create the new expense
        $expense = new Expense;
        $expense->user_id   =   $user->id;
        $expense->name      =   $request["name"];
        $expense->cost      =   $floated;
        $expense->recurring =   1;
        $expense->save();

        // Redirect with sucess message
        return redirect('expenses')->with('status', 'Recurring Expense Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // Make sure the expense exists
        $expense = Expense::findOrFail($id);

        return view('expenses.edit', [ 'expense' => $expense ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // Find the expense we're tring to update
        $expense = Expense::findOrFail($id);

        // Times the amount by 100 to store in the database
        $floated =  floatval($request["amount"]);

        // Set values to new ones
        $expense->name  =   $request['name'];
        $expense->cost  =   $floated;
        $expense->save();

        // Redirect user with success message
        return redirect('expenses')->with('status', 'Expense Update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Make sure the expense exists
        $expense = Expense::findOrFail($id);

        // Delete the expense
        $expense->delete();

        // Redirect the user to the main expenses page with success message
        return redirect('expenses')->with('status', 'Expense successfully deleted.');

    }
}
