<?php

// Helper functions

// Format Money
function formatMoney( $price )
{
    return money_format('%n', $price);
}