@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Recurring Expenses</div>

                <div class="card-body">
                    <form action="/expenses" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Rent">
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <div class="input-group-text">£</div>
                                </div>
                                <input type="text" name="amount" class="form-control" id="amount" placeholder="500">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div><!-- .card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
