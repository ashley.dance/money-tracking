@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">Edit Expense - {{ $expense->name }}</div>

                <div class="card-body">
                    <form action="/expenses/{{ $expense->id }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ $expense->name }}">
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <div class="input-group-text">£</div>
                                </div>
                                <input type="text" name="amount" class="form-control" id="amount" placeholder="Enter amount" value="{{ formatMoney($expense->cost) }}">
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" id="js-delete-expense-btn" class="btn btn-danger">Delete</button>
                        </div>
                    </form>

                    <form id="js-delete-expense-form" action="/expenses/{{ $expense->id }}" method="POST">
                        @method('DELETE')
                        @csrf
                    </form>
                </div><!-- .card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
