@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    All Recurring Expenses
                    <span>Expenses - £{{ formatMoney($totalCost) }}</span>
                </div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ( !count( $expenses ) == 0 )

                    <ul class="list-group">
                        @foreach( $expenses as $expense )
                        <li class="list-group-item d-flex justify-content-between">
                            <span>
                                {{ $expense->name }} - £{{ formatMoney($expense->cost) }}
                            </span>

                            <a class="btn btn-primary btn-sm" href="/expenses/{{ $expense->id }}/edit">Edit</a>

                        </li>
                        @endforeach
                    </ul>

                    @else
                        <p>It seems you have no expenses, add some below.</p>
                    @endif

                    <div class="mt-3">
                        <a href="/expenses/create" class="btn btn-primary">Add Expense</a>
                    </div>

                </div><!-- .card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
