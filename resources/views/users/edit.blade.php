@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit your profile</div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success mt-3">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="/users/{{ $user->id }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ $user ->name }}">
                        </div>
                        <div class="form-group">
                            <label for="amount">Monthly Income</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">£</div>
                                </div>
                                <input type="text" name="income" class="form-control" id="income" placeholder="Enter monthly income" value="{{ formatMoney($user->income) }}">
                            </div><!-- .input-group -->
                        </div><!-- .form-group -->
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>


                </div><!-- .card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
